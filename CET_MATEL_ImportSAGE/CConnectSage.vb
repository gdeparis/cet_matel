﻿Imports Objets100cLib
Public Class CConnexionSAGE
    Public BaseCpta As BSCPTAApplication100c

    Public BaseCial As BSCIALApplication100c

    Private m_PathBaseCpta As String
    Private m_PathBaseCial As String

#Region " Evènements "
    Public Event CialOpen()
    Public Event CptaOpen()

    Public Event CialClose()
    Public Event CptaClose()

#End Region

#Region " Properties "
    Public ReadOnly Property CybVersion() As String
        Get
            Return "6.0.1" '17/11/08
        End Get
    End Property

    Public ReadOnly Property xPathBaseCial() As String
        Get
            Return m_PathBaseCial
        End Get
    End Property

    Public ReadOnly Property xPathBaseCpta() As String
        Get
            Return m_PathBaseCpta
        End Get
    End Property

#End Region

#Region " Ouverture / Fermeture des objets 100 et OleDB "


    Public Sub New()

    End Sub


    Public Sub New(ByVal sFichierCompta As String, ByVal sFichierGestcom As String, ByVal sUser As String, ByVal sPassword As String)

        Try 'Connexion Cpta

            If System.IO.File.Exists(sFichierCompta) Then

                Me.BaseCpta = New BSCPTAApplication100c

                Me.BaseCpta.Name = sFichierCompta
                If sUser <> "" Then
                    Me.BaseCpta.Loggable.UserName = sUser
                    Me.BaseCpta.Loggable.UserPwd = sPassword
                End If
                Me.BaseCpta.Open()
                'Me.CptaOpen()


            End If


        Catch ex As Exception
            'WriteLog(ex.Message)
            Throw New Exception(ex.Message)
        End Try

        'Connexion Cial
        If (Not Me.BaseCpta Is Nothing AndAlso Me.BaseCpta.IsOpen) Then
        Else 'Il faut que la base de compta soit ouverte pour pouvoir ouvrir la gescom par la suite
            'WriteLog("Base de comptabilité non initialisée. Ouverture de la Gestion Commerciale '" & sFichierGestcom & "' impossible")
            Throw New Exception("Base de comptabilité non initialisée. Ouverture de la Gestion Commerciale '" & sFichierGestcom & "' impossible")
        End If

        Try 'Connexion Cial
            If System.IO.File.Exists(sFichierGestcom) Then
                m_PathBaseCial = sFichierGestcom

                BaseCial = New BSCIALApplication100c
                'Affectation de la base de compta
                Me.BaseCial.CptaApplication = Me.BaseCpta
                Me.BaseCial.Name = sFichierGestcom
                If sFichierGestcom <> "" Then
                    Me.BaseCial.Loggable.UserName = sUser
                    Me.BaseCial.Loggable.UserPwd = sPassword
                End If

                Me.BaseCial.Open()
                RaiseEvent CialOpen()

            End If
        Catch ex As Exception
            ' WriteLog(ex.Message)
        End Try
    End Sub

    Public Sub CloseAllConnections()
        CloseL100()

    End Sub
    Public Sub CloseL100()
        Try
            If Not Me.BaseCpta Is Nothing AndAlso Me.BaseCpta.IsOpen Then
                Me.BaseCpta.Close()
            End If

            If Not Me.BaseCial Is Nothing AndAlso Me.BaseCial.IsOpen Then
                Me.BaseCial.Close()
            End If
        Catch ex As Exception
        End Try
    End Sub


    Protected Overrides Sub Finalize()
        MyBase.Finalize()
        Try
            CloseL100()

        Catch ex As Exception
        End Try
    End Sub

#End Region
End Class
