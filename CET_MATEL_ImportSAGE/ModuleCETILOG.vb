﻿Imports System.IO
Imports Objets100cLib

Module ModuleCETILOG

    Public pathLog As String = Application.StartupPath & My.Settings.PathLog & "/Log_" & Now.ToString("dd_MM_yyyy_HH_mm_ss") & ".txt"

    Public Sub WriteData(ByVal strData As String, Optional append As Boolean = True)

        'Check directory first
        If Not (System.IO.Directory.Exists(Application.StartupPath & "/" & My.Settings.PathLog)) Then
            System.IO.Directory.CreateDirectory(Application.StartupPath & "/" & My.Settings.PathLog)
        End If


        Dim objReader As System.IO.StreamWriter
        Try
            objReader = New System.IO.StreamWriter(pathLog, append)
            objReader.Write("Time: " & Now() & " LOG: " & strData & vbCrLf)
            'objReader.WriteLine(vbCrLf) 'uncomment for blank line between entries
            objReader.Close()
        Catch Ex As Exception
            Throw New Exception(Ex.Message)
        End Try

    End Sub


    Public Function LireFichierSage(FileSage As String) As DataTable
        Dim dtFichierSage As New DataTable

        Try

            WriteData("Traitement du fichier " & FileSage, True)

            Dim icptColumn As Integer = 0
            For icptColumn = 0 To 41
                dtFichierSage.Columns.Add()
            Next

            icptColumn = 0
            Dim baliseTrouve As Boolean = False
            Dim drRow As DataRow = dtFichierSage.NewRow

            For Each line As String In File.ReadLines(FileSage)
                'Code here to read each line

                If line.Contains("#") AndAlso icptColumn > 0 Then
                    dtFichierSage.Rows.Add(drRow)
                    drRow = dtFichierSage.NewRow
                    baliseTrouve = False
                    icptColumn = 0
                End If

                If line = "#MECG" Then
                    'on créé la ligne dans la datatable
                    icptColumn = 0
                    baliseTrouve = True
                Else
                    If baliseTrouve Then
                        drRow(icptColumn) = line
                        icptColumn = icptColumn + 1
                    End If

                End If
            Next line



        Catch ex As Exception
            WriteData(ex.Message)
        End Try

        LireFichierSage = dtFichierSage

    End Function
    Public Sub RecupError(ByRef mP As IPMEncoder)
        Try
            'Boucle sur les erreurs contenues dans la collection
            For i As Integer = 1 To mP.Errors.Count
                'Récupération des éléments erreurs
                Dim iFail As IFailInfo = mP.Errors.Item(i)
                'récupération du numéro d'erreur, de l'indice et de la description de l'erreur
                WriteData("Code Erreur : " & iFail.ErrorCode & " Indice : " & iFail.Indice &
                " Description : " & iFail.Text)
            Next
        Catch ex As Exception
            WriteData("Erreur : " & ex.Message)
        End Try
    End Sub

    Public Function IntegrerFichierSage100(dtFichier As DataTable) As Boolean
        Try
            WriteData("Connexion à SAGE 100")
            Dim iProcess As IPMEncoder

            Dim iEcriture As IBOEcriture3

            Dim cConnect As CConnexionSAGE = New CConnexionSAGE(My.Settings.FichierCompta, My.Settings.FichierGescom, My.Settings.UserSage, My.Settings.PassSage)



            Dim sPiece As String = ""

            iProcess = cConnect.BaseCpta.CreateProcess_Encoder()

            WriteData("Intégration du fichier au processus SAGE 100")
            For Each drRowSage As DataRow In dtFichier.Select("")

                If sPiece <> drRowSage(4) Then
                    sPiece = drRowSage(4)

                    If iProcess.FactoryEcritureIn.List.Count > 0 Then
                        If iProcess.CanProcess Then
                            WriteData("Création de la pièce " & iProcess.EC_Piece)
                            iProcess.Process()

                        Else
                            WriteData("Erreur lors de la création de la pièce " & iProcess.EC_Piece)
                            RecupError(iProcess)
                        End If

                        iProcess = cConnect.BaseCpta.CreateProcess_Encoder()
                        iProcess.Journal = cConnect.BaseCpta.FactoryJournal.ReadNumero(drRowSage(0))
                        iProcess.Date = DateTime.ParseExact(drRowSage(1), "dMy", Nothing)
                        iProcess.EC_Piece = drRowSage(3)
                        iProcess.EC_Intitule = drRowSage(10)

                    Else
                        iProcess = cConnect.BaseCpta.CreateProcess_Encoder()
                        iProcess.Journal = cConnect.BaseCpta.FactoryJournal.ReadNumero(drRowSage(0))
                        iProcess.Date = DateTime.ParseExact(drRowSage(1), "dMy", Nothing)
                        iProcess.EC_Piece = drRowSage(3)
                        iProcess.EC_Intitule = drRowSage(10)

                    End If
                End If




                iEcriture = iProcess.FactoryEcritureIn.Create()
                'iEcriture.Date = drRowSage(1)

                'iEcriture.EC_Piece = iProcess.Journal.NextEC_Piece(drRowSage(1))

                iEcriture.EC_RefPiece = drRowSage(4)
                iEcriture.EC_Reference = drRowSage(32)
                iEcriture.CompteG = cConnect.BaseCpta.FactoryCompteG.ReadNumero(drRowSage(6))

                If drRowSage(7) <> "" Then
                    iEcriture.CompteGContrepartie = cConnect.BaseCpta.FactoryCompteG.ReadNumero(drRowSage(7))
                End If

                If drRowSage(8) <> "" Then
                    iEcriture.Tiers = cConnect.BaseCpta.FactoryTiers.ReadNumero(drRowSage(8))
                End If

                If drRowSage(9) <> "" Then
                    iEcriture.TiersContrepartie = cConnect.BaseCpta.FactoryTiers.ReadNumero(drRowSage(9))
                End If

                iEcriture.EC_Intitule = drRowSage(10)

                If drRowSage(12) <> "" Then
                    iEcriture.EC_Echeance = DateTime.ParseExact(drRowSage(12), "dMy", Nothing)
                End If

                If drRowSage(16) > 0 Then
                    iEcriture.EC_Sens = EcritureSensType.EcritureSensTypeCredit
                Else
                    iEcriture.EC_Sens = EcritureSensType.EcritureSensTypeDebit
                End If

                iEcriture.EC_Montant = drRowSage(17)

                iEcriture.WriteDefault()

            Next

            If iProcess.CanProcess Then
                WriteData("Création de la pièce " & iProcess.EC_Piece)
                iProcess.Process()

            Else
                WriteData("Erreur lors de la création de la pièce " & iProcess.EC_Piece)
                RecupError(iProcess)
            End If

            cConnect.CloseAllConnections()
        Catch ex As Exception
            WriteData(ex.Message)
        End Try
    End Function

End Module
