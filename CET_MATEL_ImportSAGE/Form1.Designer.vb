﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txLog = New System.Windows.Forms.TextBox()
        Me.OFD = New System.Windows.Forms.OpenFileDialog()
        Me.txFile = New System.Windows.Forms.TextBox()
        Me.btParcourir = New System.Windows.Forms.Button()
        Me.btImport = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btImport)
        Me.GroupBox1.Controls.Add(Me.btParcourir)
        Me.GroupBox1.Controls.Add(Me.txFile)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(786, 113)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Sélection du fichier à importer"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txLog)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 131)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(786, 460)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Log de traitement"
        '
        'txLog
        '
        Me.txLog.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txLog.Location = New System.Drawing.Point(6, 19)
        Me.txLog.Multiline = True
        Me.txLog.Name = "txLog"
        Me.txLog.ReadOnly = True
        Me.txLog.Size = New System.Drawing.Size(774, 435)
        Me.txLog.TabIndex = 0
        '
        'OFD
        '
        Me.OFD.FileName = "OpenFileDialog1"
        '
        'txFile
        '
        Me.txFile.Location = New System.Drawing.Point(18, 29)
        Me.txFile.Name = "txFile"
        Me.txFile.Size = New System.Drawing.Size(631, 20)
        Me.txFile.TabIndex = 0
        '
        'btParcourir
        '
        Me.btParcourir.Location = New System.Drawing.Point(665, 29)
        Me.btParcourir.Name = "btParcourir"
        Me.btParcourir.Size = New System.Drawing.Size(115, 23)
        Me.btParcourir.TabIndex = 1
        Me.btParcourir.Text = "Parcourir"
        Me.btParcourir.UseVisualStyleBackColor = True
        '
        'btImport
        '
        Me.btImport.Location = New System.Drawing.Point(665, 84)
        Me.btImport.Name = "btImport"
        Me.btImport.Size = New System.Drawing.Size(115, 23)
        Me.btImport.TabIndex = 2
        Me.btImport.Text = "Importer"
        Me.btImport.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(810, 603)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "Form1"
        Me.RightToLeftLayout = True
        Me.Text = "Intégration des fichiers SAGE 100 Comptabilité"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents txLog As TextBox
    Friend WithEvents btParcourir As Button
    Friend WithEvents txFile As TextBox
    Friend WithEvents OFD As OpenFileDialog
    Friend WithEvents btImport As Button
End Class
